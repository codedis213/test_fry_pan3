# -*- coding: UTF-8 -*-

import scrapy

from tutorial.items import DmozItem
from scrapy.selector import Selector
from bs4 import BeautifulSoup
import re
import json
import ast


class DmozSpider(scrapy.Spider):
    name = "dmoz"
    allowed_domains = ["www.korasoi.com"]
    start_urls = [
        "http://www.korasoi.com/blog/",
    ]

    def parse(self, response):
        page = response.body
        soup = BeautifulSoup(page, "html.parser")
        cate = soup.find("aside", attrs={"id":"categories-7"}).find_all("li")

        for li in cate:
            if li.find("a"):
                url = str(li.find("a").get("href")).strip()
                yield scrapy.Request(url, callback=self.parse_dir_contents)


    def parse_dir_contents(self, response):
        page2 = response.body
        soup2 = BeautifulSoup(page2, "html.parser")
        article_list = soup2.find_all("article", attrs={"id":re.compile("post-\d*")})

        for article in article_list:
            item_link = str(article.find("h2", attrs={"class":"entry-title"}).find("a").get("href")).strip()
            yield scrapy.Request(item_link, callback=self.parse_item_page)


    def parse_item_page(self, response):
        item = DmozItem()
        page3 = response.body
        soup3 = BeautifulSoup(page3, "html.parser")
        item["domain"] = "http://www.korasoi.com"
        item["itemurl"] = response.url
        cat_list = soup3.find("div", attrs={"id":"primary"}).find("div", attrs={"class":"entry-cats"})
        cat_list = cat_list.find_all("a")

        # item["categoriestags"] = dict((str(cate.get_text()).strip(), str(cate.get("href")).strip())
        #     for cate in cat_list)
        item["categoriestags"] = [str(cate.get_text()).strip() for cate in cat_list]

        item["title"] = soup3.find("h1", attrs={"class":"entry-title"}).get_text()
        author_tag = soup3.find("div", attrs={"class":"entry-author"}).find("a")
        item["author"] = author_tag.get_text()
        item["authorlink"] = author_tag.get("href")
        item["serves"] = soup3.find("p", text=re.compile(r".*Serves\s\d.*"))

        if item["serves"]:
            item["serves"] = soup3.find("p", text=re.compile(r".*Serves\s\d.*")).get_text()

        item["imglink"] = soup3.find("img", attrs={"class":"attachment-post-thumbnail wp-post-image"}).get("src")
        
        try:
            item["ingredients"] = soup3.find("strong", text=re.compile("Ingredients")).find_parent("p").find_next("p").get_text().encode('ascii', 'ignore')
        except:
            item["ingredients"] = soup3.find("span", text=re.compile("Ingredients")).find_parent("p").find_next("p").get_text()
        
        yield item